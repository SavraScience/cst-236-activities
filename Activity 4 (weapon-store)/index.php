<?php

    // imports
    use misd\web\Controller;

    // configuration file
    require_once "_config/a_config.php";
    
    // page metadata
    $_ENV[Controller::CURR_PAGE_ID] = "Home";
    $_ENV[Controller::CURR_PAGE_TITLE] = "Weapon Store - Home";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
        ?>
	</head>
	<body>
		<div class="page-wrap">
		
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
    		
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Home</h2>
    		<h3>Weapons</h3>
			<?php Controller::requireOnce('inc/page-parts/weapons-container.php'); ?>
		</div>
		
		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>	
		
		<!-- Scripts -->
		<script src="<?php echo Controller::resolvePath("js/weapons.js"); ?>"></script>
	</body>
</html>