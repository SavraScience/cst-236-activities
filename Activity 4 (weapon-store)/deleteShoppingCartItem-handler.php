<?php

    // imports
    require_once 'autoloader.php';    
    use misd\web\Controller;
    use inc\web\WeaponStoreCache;
    use inc\models\ShoppingCartLineItemModel;
    use inc\data\ShoppingCartDao;
    
    // CONSTANTS
    define('TIMID', 'timid');
    
    // get post input
    if (isset($_POST[TIMID]))    
    {
        $timid = $_POST[TIMID];
        //echo "POST timid = $timid";
        
        // resolve cart item
        $cartItem = new ShoppingCartLineItemModel();
        $cartItem->setTempId($timid);
        $cartItem = WeaponStoreCache::resolve(WeaponStoreCache::SESSKEY_CART, $cartItem);
        
        /** @var $cartItem ShoppingCartLineItemModel */
        if ($cartItem->getId() > 0)
        {

            // $cartItem resolved in Session cache
            // -- attempt to delete the item in the shopping cart
            $dao = new ShoppingCartDao();
            if ($dao->deleteById($cartItem->getId()))
            {
                // update the cart in session cache
                WeaponStoreCache::remove(WeaponStoreCache::SESSKEY_CART, $cartItem);
            }
            else
            {
                //echo "Delete failed.";
            }
        }
        else
        {
            //echo "Cart Item could not be resolved...";
        }
    }

    // reload shopping cart
    Controller::requireOnce('loadShoppingCart.php');

?>