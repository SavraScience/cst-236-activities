<?php

    // imports
    require_once 'autoloader.php';
    require_once 'inc/misc-functions.php';
    use inc\models\UserModel;
    use misd\security\InputSanitizer;
    use inc\data\UserDAO;
    use misd\security\SecurityService;
    use misd\web\Controller;
    
    /**
     * A registration handler that processes the data on the registration form
     * and redirects the user as necessary
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @todo Validate and sanitize input before deploying!
     */
    
    // sanitize input
    InputSanitizer::sanitizeStringArray($_POST);

    /* LEGEND */
    # - login info -
    # txtUsername
    
    // collect the data on the page
    $un = $_POST['txtUsername'];
    
    // create a new user from the form data
    $newUser = new UserModel();
    $newUser->setUsername($un);
    
    // insert new user into the database
    $dao = new UserDAO();
    $result = $dao->insert($newUser);
    
    if ($result)
    {   
        // login and redirect the user
        $security = new SecurityService($newUser->getUsername(), "");
        if ($security->login()) Controller::redirect('index');
    }
    console_log("Oops!  Something went wrong...");
?>