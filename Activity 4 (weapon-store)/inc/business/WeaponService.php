<?php
namespace inc\business;

use inc\data\WeaponDao;
use misd\models\AbstractObjectModel;

require_once ('inc/business/BusinessService.php');

/**
 *
 * @author djsav
 *        
 */
class WeaponService extends BusinessService
{
    // CONSTRUCTOR
    public function __construct()
    {
        parent::__construct();
    }
    
    // OVERRIDDEN METHODS
    /**
     * 
     * {@inheritDoc}
     * @see \inc\business\BusinessService::getAll()
     */
    public function getAll()
    {
        $dao = new WeaponDao();
        return $dao->findAll();
    }

    /**
     * 
     * {@inheritDoc}
     * @see \inc\business\BusinessService::findById()
     */
    public function findById(int $id): AbstractObjectModel
    {
        $dao = new WeaponDao();
        return $dao->findById($id);
    }

    /**
     * 
     * {@inheritDoc}
     * @see \inc\business\BusinessService::update()
     */
    public function update(AbstractObjectModel $weapon): bool
    {
        $dao = new WeaponDao();
        return $dao->update($weapon);
    }

    /**
     * 
     * {@inheritDoc}
     * @see \inc\business\BusinessService::delete()
     */
    public function delete(AbstractObjectModel $object): int
    {
        $dao = new WeaponDao();
        return (int) $dao->deleteById($object->getId());
    }
}

