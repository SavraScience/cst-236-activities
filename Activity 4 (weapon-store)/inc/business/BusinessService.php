<?php
namespace inc\business;


use misd\models\AbstractObjectModel;

abstract class BusinessService
{
    // INSTANCE VARIABLES
    
    // CONSTRUCTOR
    public function __construct() {}
    
    // PUBLIC METHODS
    public abstract function getAll();
    public abstract function findById(int $id) : AbstractObjectModel;
    public abstract function update(AbstractObjectModel $object) : bool;
    public abstract function delete(AbstractObjectModel $object) : int;
}

