<?php
namespace inc\business;

use inc\data\UserDao;
use misd\models\AbstractObjectModel;

require_once ('inc/business/BusinessService.php');

/**
 *
 * @author djsav
 *        
 */
class UserService extends BusinessService
{
    // CONSTRUCTOR
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $dao = new UserDao();
        return $dao->findAll();
    }

    // PUBLIC METHODS
    /**
     * (non-PHPdoc)
     *
     * @see \inc\business\BusinessService::findById()
     */
    public function findById($id)
    {
        $dao = new UserDao();
        return $dao->findById($id);
    }

    /**
     * (non-PHPdoc)
     *
     * @see \inc\business\BusinessService::delete()
     */
    public function delete(AbstractObjectModel $object)
    {
        $dao = new UserDao();
        return $dao->deleteById($object->getId());
    }

    /**
     * (non-PHPdoc)
     *
     * @see \inc\business\BusinessService::update()
     */
    public function update($object)
    {
        $dao = new UserDao();
        return $dao->update($object);
    }
}

