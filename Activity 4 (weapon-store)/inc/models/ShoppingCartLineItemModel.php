<?php
namespace inc\models;

require_once ('misd/models/AbstractObjectModel.php');

use misd\models\AbstractObjectModel;

class ShoppingCartLineItemModel extends AbstractObjectModel
{
    // INSTANCE VARIABLES
    private $userId;
    private $weaponId;
    private $quantity;
    
    // CONSTRUCTOR
    public function __construct($id = null)
    {
        parent::__construct($id);
    }
    
    // PUBLIC ACCESSOR/MUTATOR METHODS
    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getWeaponId()
    {
        return $this->weaponId;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @param mixed $weaponId
     */
    public function setWeaponId($weaponId)
    {
        $this->weaponId = $weaponId;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}

