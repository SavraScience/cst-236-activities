<?php
namespace inc\models;

// imports
use misd\models\AbstractObjectModel;

/**
 * 
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class UserModel extends AbstractObjectModel
{   
    // INSTANCE VARIABLES
    private $username;
    private $points;
    
    // CONSTRUCTOR
    public function __construct(int $id = null)
    {
        parent::__construct($id);
    }
    
    // PUBLIC ACCESSOR/MUTATOR METHODS
    /**
     * @return string The user's username
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * @param string $username The user's username
     */
    public function setUsername(string $username)
    {
        // validation
        
        
        $this->username = $username;
    }
    
    /**
     * @return mixed
     */
    public function getPoints() : ?int
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     */
    public function setPoints(?int $points)
    {
        $this->points = $points;
    }

    // OVERRIDDEN METHODS
    public function __toString()
    {
        return <<< ML
            <p>
            [UserModel class]<br>\n
            ID: $this->id<br>\n
            Username: $this->username<br>\n
            </p>
ML;
    }
    
}

?>