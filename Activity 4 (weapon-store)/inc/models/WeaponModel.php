<?php
namespace inc\models;

use misd\models\AbstractObjectModel;

/**
 *
 * @author djsav
 *        
 */
class WeaponModel extends AbstractObjectModel
{
    // INSTANCE VARIABLES
    private $description;
    private $pointCost;
    
    // EMPTY CONSTRUCTOR
    public function __construct($id = null)
    {
        parent::__construct($id);
    }
    // ACCESSOR/MUTATOR METHODS
    /**
     * @return mixed
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getPointCost() : int
    {
        return $this->pointCost;
    }

    /**
     * @param mixed $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $pointCost
     */
    public function setPointCost(int $pointCost)
    {
        $this->pointCost = $pointCost;
    }

}

