<?php

    function array_toString(array $objects)
    {
        $toString = "";
        if (is_array($objects))
        {
            foreach ($objects as $key => $val)
            {
                $type = gettype($val);
                $toString .= "['$key'] => ($type) $val \n";
            }
            return $toString;
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Prints a JavaScript console.log($output) to the page,
     * which effectively logs the outpout to the browser window. 
     * 
     * Source: https://stackify.com/how-to-log-to-console-in-php/
     * 
     * @author Kim Sia
     * @param string $output
     * @param boolean $with_script_tags
     */
    function console_log(string $output, bool $with_script_tags = true) {
        $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) .
        ');';
        if ($with_script_tags) {
            $js_code = '<script>' . $js_code . '</script>';
        }
        echo $js_code;
    }
    
    /**
     * Replaces Windows backslashes with webserver-safe
     * forward-slashes for path resolution
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $path
     * @return mixed
     */
    function switchSlashes(string $path)
    {
        return str_replace('\\', '/', $path);
    }
    
    /**
     * Tests to see if an object can be converted to a string
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param mixed $var The object or variable you want to test
     * @return bool
     */
    function stringConvertable($var) : bool
    {
        if (is_scalar($var))
        {
            return true;
        }
        elseif (is_array($var))
        {
            return false;
        }
        elseif (is_object($var))
        {
            return method_exists($var, '__toString');
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Utilizes PHP cURL library to retrieve file data from a URL.
     * This can then be used in conjunction with file_put_contents
     * to copy a file from the internet to the local server.
     * 
     * Source: https://www.geeksforgeeks.org/saving-an-image-from-url-in-php/
     * 
     * @author feduser
     * @param string $url The URL of the resource you want to copy locally
     * @return mixed
     */
    function file_get_contents_curl(string $url)
    {
        console_log("Attempting to retrieve data stream from URL: '$url'");
        
        // intialize cURL and set options
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        
        // retrieve the data
        $data = curl_exec($ch);
        
        // debugging
        $result = curl_getinfo($ch);
        foreach ($result as $key => $msg)
        {
            if (!is_null($msg))
            {
                //console_log("[$key] => $msg");
            }
            
        }
        
        // close the cURL channel
        curl_close($ch);
        
        return $data;
    }
    
    function isValidImageType($pathOrExtension) : bool
    {
        // TODO: Implement!
        
        // -- if begins with period and it is the only period
        
        // else get file extension
        
        return true;
    }
?>