<?php
namespace inc\security;

// imports
use inc\models\UserModel;
use misd\security\CurrentUserInterface;

class CurrentUser implements CurrentUserInterface
{
    // INSTANCE VARIABLES
    private $userId = 0;
    private $username = "";
    private $points = 0;
    
    // CONSTRUCTOR
    public function __construct(UserModel $user) 
    {
        // TODO: Add email?
        $this->userId = $user->getId();
        $this->username = $user->getUsername();
        $this->points = $user->getPoints();
    }
    
    // ACCESSOR/MUTATOR METHODS
    public function getUserId()
    {
        return $this->userId;
    }
    
    public function getUsername() : string
    {
        return $this->username;
    }
    
    public function getPoints() : int
    {
        return $this->points;
    }
}

