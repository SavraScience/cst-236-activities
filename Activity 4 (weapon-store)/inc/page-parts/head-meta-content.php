<?php 
    
    // imports
    use misd\web\Controller;

?>
<title><?php echo $_ENV[Controller::CURR_PAGE_TITLE]; ?></title>

<?php // change href path to remove MeNursery after you have deployed to the web ?>

<!-- Bootstrap 4 Library -->
<link rel="stylesheet" type="text/css" href="/weapon-store/bootstrap-4.3.1-dist/css/bootstrap.min.css">

<!-- DataTables Library -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<!-- Custom CSS File -->
<link rel="stylesheet" type="text/css" href="/weapon-store/css/main.css?nocache=true">

<!-- META TAGS -->
<meta name="viewport" content="width=device-width, initial-scale=1">