<?php
    
/**
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */

// IMPORTS
require_once 'autoloader.php';
use misd\security\SecurityService;
use misd\web\Controller;
use misd\web\Webpage;
use inc\data\ShoppingCartDao;
use inc\web\WeaponStoreCache;

// PAGE DEFINITIONS
// --page constants
define("HOME_PAGE", "Home");
define("LOGIN_PAGE", "Login");
define("REGISTER_PAGE", "Register");

// include webpages and their paths here:
$pages = array();
$pages[HOME_PAGE] = new Webpage(HOME_PAGE, 'index.php');

// login and registration links ONLY
$authPages = array();
$authPages[LOGIN_PAGE] = new Webpage(LOGIN_PAGE, 'login.php');
$authPages[REGISTER_PAGE] = new Webpage(REGISTER_PAGE, 'register.php');

// now print each page as a navigation link to the screen...
?>
<nav>
    <ul>
    	<?php 
    	
    	   # Dynamically builds the links on the page
    	   foreach($pages as $page)
    	   {
    	       $link = '<a href="';
    	       if(Webpage::isCurrentPage($page))
    	           $link .= '" class="currentpage">'; 
    	       else 
    	           $link .= $page->getPath() . '">';
    	       $link .= "<li>" . $page->getName() . "</li></a>";
    	       echo $link;
    	   }
    	?>	
    </ul>
    <ul>
    	<?php
    	
    	   # Dynamically builds login/logout and register links on the page
    	   if (SecurityService::isCurrUserLoggedIn())
    	   {
    	       // display logout link only
    	       $link = '<a href="';
    	       $link .= Controller::resolvePath("logout.php");
    	       $link .= '"><li>Logout</li></a>';
    	       echo $link;
    	   }
    	   else
    	   {
    	       // display login and registration links
        	   foreach($authPages as $page)
        	   {
        	       echo "<a href=\"";
        	       if(Webpage::isCurrentPage($page))
        	           echo "#\" class=\"currentpage\">"; 
        	       else 
        	           echo $page->getPath() . "\">";
        	       echo "<li>" . $page->getName() . "</li></a>";
        	   }
    	   }
    	?>	    	
    </ul>
	<?php 
	
    	if (SecurityService::isCurrUserLoggedIn())
    	{
    	    // get CurrentUser info
            $currUser = SecurityService::getCurrentUser();
            $username = $currUser->getUsername();
            $points = $currUser->getPoints();
            
    	    // display user name & points
    	    echo <<<ML
                <div class="profile">
                    <h4>Hello, $username!</h4>
                    <span class="badge-pill badge-success point-balance">Point Balance: <span id="user-points">$points<span></span>
                </div>
ML;
    	}
	
	?>
	<div class="clear-float"><!-- EMPTY DIV --></div>
	<div class="cart">
		<?php echo Controller::requireOnce('loadCartIcon.php'); ?>
	</div>
</nav>