<?php
    
    // imports    
    use inc\business\WeaponService;
    use misd\security\SecurityService;
    use inc\web\WeaponStoreCache;
    use inc\data\ShoppingCartDao;
use inc\models\ShoppingCartLineItemModel;
    
?>

<div id="weapons-container">
	<?php 
    
	// initialize variables
	/** @var $cart ShoppingCartLineItemModel[] */
	$currUser = null;
	$cart = null;
	
	// load all weapons
	$service = new WeaponService();
	$weapons = $service->getAll();
	
	// store results in session cache
	WeaponStoreCache::register(WeaponStoreCache::SESSKEY_WEAPONS, $weapons);
	
	if (SecurityService::isCurrUserLoggedIn())
	{
	    // get current user
	    $currUser = SecurityService::getCurrentUser();
	    
	    if (!is_null($currUser))
	    {
	        // load all of the CurrentUser's cart items
	        console_log("Current user is logged in! Loading cart items...");
	        $dao = new ShoppingCartDao();
	        $cart = $dao->getCartForUserId($currUser->getUserId());
	    }
	    else
	    {
	        // debugging
	        console_log("User is logged in, but could not retrieve current user");
	    }
	}
	else
	{
	    // debugging
	    console_log("Current user is not logged in");
	}
	
	if (!empty($weapons))
	{
    	 foreach ($weapons as $weapon)
    	 {
    	     /** @var $weapon \inc\models\WeaponModel */
    	     $tempId = $weapon->getTempId();
    	     $description = $weapon->getDescription();
    	     $cost = $weapon->getPointCost();
    	     
    	     // check to see if weapon is in current user's cart
    	     $inCart = false;
            if (!is_null($cart))
            {
                foreach($cart as $cartItem)
                {
                    if ($weapon->getId() == $cartItem->getWeaponId())
                        $inCart = true;
                }
            }
    	     
            $addlClass = "";
            if ($inCart) $addlClass = " in-cart";
            
    	     echo <<<ML
                <div id="$tempId" class="card$addlClass">
                    <div class="card-body">
                        <h5 class="card-title">$description</h5>
                        <div class="weapon-body">
                            <div class="weapon-desc">
                                <label class="weapon-lbl">Cost:</label>
                                <span class="weapon-val">$cost points</span></div>
ML;
    	     // default button class, message, and disabled status
    	     $btnMsg = "Add to Cart";
    	     $btnClass = " btn-success add-to-cart-btn";
    	     $btnDisabled = "";
    	     if ($inCart)
    	     {
    	         // change button class & message
    	         $btnMsg = "In Cart";
    	         $btnClass = " btn-warning add-to-cart-btn";
    	         $btnDisabled = " disabled";
    	     }
    	     
    	     if (SecurityService::isCurrUserLoggedIn())
    	     {
    	         echo <<<ML
                    <form action="addToCart-handler.php" method="post" class="cart-item-form">
                        <input name="itemId" type="hidden" value="$tempId" />
                        <button type="button" class="btn$btnClass"$btnDisabled>$btnMsg</button>
                    </form>
ML;
    	     }
    	     echo <<<ML
                    </div> <!-- END class="weapons-body" -->
                </div> <!-- END class="card-body" -->
            </div> <!-- END class="card" -->
ML;
    	 }
	}
	?>
</div>