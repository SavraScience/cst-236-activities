<?php

namespace inc\data;

// imports
require_once 'autoloader.php';
require_once 'inc/misc-functions.php';
use inc\models\UserModel;
use misd\data\Dao;

/**
 * Data Access Object for the User table
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class UserDao extends Dao
{
    // CONSTANTS
    
    // -- table name
    //const TBL_NAME = 'app_user';
    private const TBL_NAME = 'app_user';
    
    // -- field names
    private const FLD_NAME_ID = 'user_id';
    private const FLD_NAME_USERNAME = 'username';
    private const FLD_NAME_POINTS = 'points';
    
    // CONSTRUCTOR
    function __construct()
    {
        parent::__construct(self::TBL_NAME, new UserModel());
        $this->setMapProperties();
    }
    
    // IMPLEMENTED METHODS
    final function setMapProperties()
    {
        // map object/class properties of the model to the database
        $translator = &parent::getDataTranslator();
        $translator->addTableMapping("id", self::FLD_NAME_ID, self::TBL_NAME);
        $translator->addTableMapping("username", self::FLD_NAME_USERNAME, self::TBL_NAME);
        $translator->addTableMapping("points", self::FLD_NAME_POINTS, self::TBL_NAME);
    }

    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findById()
     */
    public function findById($id)
    {
        $record = parent::findById($id);
        if (!is_null($record))
        {
            return $this->instantiateObject($record);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findAll()
     */
    public function findAll()
    {
        $rsUsers = parent::findAll();
        return $this->instantiateObjects($rsUsers);
    }
    
    public function find($queryObject)
    {
        $rsUsers = parent::find($queryObject);
        return $this->instantiateObjects($rsUsers);
    }
}

