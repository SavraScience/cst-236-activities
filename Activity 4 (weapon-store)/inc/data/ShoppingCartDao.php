<?php
namespace inc\data;

require_once ('misd/data/Dao.php');

use misd\data\Dao;
use misd\data\DatabaseManager;
use inc\models\ShoppingCartLineItemModel;

class ShoppingCartDao extends Dao
{
    // CONSTANTS
    
    // -- table name
    //const TBL_NAME = 'app_user';
    private const TBL_NAME = 'shopping_cart';
    
    // -- field names
    private const FLD_NAME_ID = 'cart_item_id';
    private const FLD_NAME_USR_ID = 'user_id';
    private const FLD_NAME_WEAPON_ID = 'weapon_id';
    private const FLD_NAME_QTY = 'qty';
    
    // CONSTRUCTOR
    /**
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tableName
     *            The name of the table in
     *            the database that corresponds to the class/entity
     *            you wish to perform CRUD operations on
     */
    public function __construct()
    {
        parent::__construct(self::TBL_NAME, new ShoppingCartLineItemModel());
    }

    protected function setMapProperties()
    {
        // map object/class properties of the model to the database
        $translator = &parent::getDataTranslator();
        $translator->addTableMapping("id", self::FLD_NAME_ID, self::TBL_NAME);
        $translator->addTableMapping("userId", self::FLD_NAME_USR_ID, self::TBL_NAME, false);
        $translator->addTableMapping("weaponId", self::FLD_NAME_WEAPON_ID, self::TBL_NAME, false);
        $translator->addTableMapping("quantity", self::FLD_NAME_QTY, self::TBL_NAME);
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findById()
     */
    public function findById($id)
    {
        $record = parent::getById($id);
        if (!is_null($record))
        {
            return $this->instantiateObject($record);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findAll()
     */
    public function findAll()
    {
        $rsCartItems = parent::findAll();
        return $this->instantiateObjects($rsCartItems);
    }
    
    public function find($queryObject)
    {
        $rsCartItems = parent::find($queryObject);
        return $this->instantiateObjects($rsCartItems);
    }
    
    public function getCartForUserId(int $id)
    {
        $tbl = self::TBL_NAME;
        $fldUserId = self::FLD_NAME_USR_ID;
        $sql = <<<ML
            SELECT *
            FROM $tbl
            WHERE $fldUserId = ?
            ;
ML;
        DatabaseManager::persistConnection($this->conn);
        
        // create a prepared statement
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        
        $rsCartItems = $stmt->get_result();
        return $this->instantiateObjects($rsCartItems);
    }
    
    public function deleteCartForUserId(int $id) : bool
    {
        $tbl = self::TBL_NAME;
        $fldUserId = self::FLD_NAME_USR_ID;
        $sql = <<<ML
            DELETE
            FROM $tbl
            WHERE $fldUserId = ?
            ;
ML;
        DatabaseManager::persistConnection($this->conn);
        
        // create a prepared statement
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("i", $id);
        return $stmt->execute();
    }
}

