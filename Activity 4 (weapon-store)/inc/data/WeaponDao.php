<?php
namespace inc\data;

require_once ('misd/data/Dao.php');

use misd\data\Dao;
use inc\models\WeaponModel;

class WeaponDao extends Dao
{
    // CONSTANTS
    
    // -- table name
    //const TBL_NAME = 'app_user';
    private const TBL_NAME = 'weapon';
    
    // -- field names
    private const FLD_NAME_ID = 'weapon_id';
    private const FLD_NAME_DESC = 'weapon_desc';
    private const FLD_NAME_PT_COST = 'point_cost';
    
    // CONSTRUCTOR
    /**
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tableName
     *            The name of the table in
     *            the database that corresponds to the class/entity
     *            you wish to perform CRUD operations on
     */
    public function __construct()
    {
        parent::__construct(self::TBL_NAME, new WeaponModel());
    }

    // OVERRIDDEN METHODS
    /**
     * (non-PHPdoc)
     *
     * @see \misd\data\Dao::setMapProperties()
     */
    protected function setMapProperties()
    {
        // map object/class properties of the model to the database
        $translator = &parent::getDataTranslator();
        $translator->addTableMapping("id", self::FLD_NAME_ID, self::TBL_NAME);
        $translator->addTableMapping("description", self::FLD_NAME_DESC, self::TBL_NAME);
        $translator->addTableMapping("pointCost", self::FLD_NAME_PT_COST, self::TBL_NAME);
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findById()
     */
    public function findById($id)
    {
        $record = parent::findById($id);
        if (!is_null($record))
        {
            return $this->instantiateObject($record);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findAll()
     */
    public function findAll()
    {
        $rsWeapons = parent::findAll();
        return $this->instantiateObjects($rsWeapons);
    }
    
    public function find($queryObject)
    {
        $rsWeapons = parent::find($queryObject);
        return $this->instantiateObjects($rsWeapons);
    }
}

