<?php

namespace inc\web;

require_once ('misd/web/SessionCache.php');

use misd\web\SessionCache;

class WeaponStoreCache extends SessionCache
{
    // session cache constants
    public const SESSKEY_CART = 'cart';
    public const SESSKEY_WEAPONS  = 'weapons';
}

