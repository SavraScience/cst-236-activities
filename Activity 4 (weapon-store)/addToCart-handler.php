<?php
    
    // imports
    require_once 'autoloader.php';
    use inc\web\WeaponStoreCache;
    use inc\models\WeaponModel;
    use inc\data\ShoppingCartDao;
    use inc\models\ShoppingCartModel;
    use misd\security\SecurityService;
    use misd\web\SessionCache;
    use inc\models\ShoppingCartLineItemModel;
    
    SessionCache::persistSession();
    
    // CONSTANTS
    define('CTL_TIMID', 'timid');    

    // fetch the clicked item
    if (isset($_POST[CTL_TIMID]))
    {
        $timid = $_POST[CTL_TIMID];
        
        // resolve the temporary ID to the session cache
        $weapon = new WeaponModel();
        $weapon->setTempId($timid);
        $weapon = WeaponStoreCache::resolve(WeaponStoreCache::SESSKEY_WEAPONS, $weapon);
        
        /** @var $weapon WeaponModel */
        $weaponId = $weapon->getId();
        if ($weaponId > 0)
        {
            // weapon resolved
            // -- initialize variables
            $cartItem = new ShoppingCartLineItemModel();
            // -- check to see if a shopping cart exists in the session
            $cart = WeaponStoreCache::get(WeaponStoreCache::SESSKEY_CART);
            
            if (is_null($cart))
            {
                // -- create new "Shopping Cart"
                $cart = array();
            }
                        
            // -- create new "Cart Item"
            $cartItem->setWeaponId($weaponId);
            $cartItem->setQuantity(1);
            
            // -- get the current user
            $user = SecurityService::getCurrentUser();
            if (!is_null($user))
            {
                //console_log("\$user is type of " . get_class($user));
                $cartItem->setUserId($user->getUserId());
                
                // save the item to the cart (in the DB)
                $dao = new ShoppingCartDao();
                if ($dao->insert($cartItem))
                {
                    // add the cart item to the in-memory cart
                    array_push($cart, $cartItem);
                    echo true;
                    exit(0);
                }
                else
                {
                    echo false;
                    exit(0);
                }
            }
            else
            {
                // user is not currently logged in
                // -- add the cart item to the in-memory cart
                array_push($cart, $cartItem);
            }
        }
        else
        {
            echo false;
            exit(0);
        }
    }
?>