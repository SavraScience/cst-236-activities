<?php
    
    /**
     * A simple login form
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @todo Validate and sanitize input before deploying!
     */

    // imports
    use misd\web\Controller;

    // configuration file
    require_once "_config/a_config.php";
    
    // page metadata
    $_ENV[Controller::CURR_PAGE_ID] = "Login";
    $_ENV[Controller::CURR_PAGE_TITLE] = "Weapon Store - Login";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php"); 
        ?>
	</head>
	<body>
		<div class="page-wrap">
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Login</h2>
    		<hr />
    		<form method="post" action="<?php echo Controller::resolvePath('login-handler'); ?>" autocomplete="off">
    			<div class="login-container">
    				<label class="username-lbl">Username</label>
    				<input name="txtUsername" id="username" class="username-txtbox" type="text" required autofocus="autofocus" />
    				<label class="password-lbl">Password</label>
    				<input name="txtPassword" id="password" class="password-txtbox" required type="password" />
    				<button type="submit" class="btn btn-primary login-button">Login</button>
    			</div>
    		</form>
		</div>
		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>			
	</body>
</html>