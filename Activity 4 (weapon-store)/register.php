<?php

    /**
     * A registration form for new users
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @todo Validate and sanitize input before deploying!
     */

    // imports
    use misd\web\Controller;

    // configuration file
    require_once "_config/a_config.php";
    
    // page metadata
    $_ENV[Controller::CURR_PAGE_ID] = "Register";
    $_ENV[Controller::CURR_PAGE_TITLE] = "MeNusery - Register";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
        ?>
        <!-- change link? -->
        <link rel="stylesheet" type="text/css" href="/weapon-store/css/registration.css" />
	</head>
	<body>
		<div class="page-wrap">
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Register</h2>
    		<hr />
    		<form id="registration-form" method="post" action="register-handler.php">
    			<div id="registration-container">
        			
        			<!-- Username -->
        			<label id="lbl-username" for="txt-username">Username</label>
        			<input id="txt-username" name="txtUsername" type="text" class="form-ctl" required />
        			
        			<button id="btn-register" type="submit" class="btn btn-primary">Register</button>
    			</div>
    		</form>
		</div>
		
		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>			
	</body>
</html>