<?php

// imports
use misd\web\Controller;
use misd\security\SecurityService;

// configuration file
require_once "_config/a_config.php";

// page metadata
$_ENV[Controller::CURR_PAGE_ID] = "Home";
$_ENV[Controller::CURR_PAGE_TITLE] = "Weapon Store - Home";

// get current user
$currUser = SecurityService::getCurrentUser();
?>

<!DOCTYPE html>
<html>
	<head>
		<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
        ?>
	</head>
	<body>
		<div class="page-wrap">
		
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
    		
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
			<div class="jumbotron jumbotron-fluid clear-float">
				<div class="container">
    				<h2>Order Confirmation</h2>
    				<h4>Thank you for your purchase<?php if (!is_null($currUser)) echo ", " . $currUser->getUsername(); ?>!</h4>
    				<?php 
    				    if (!is_null($currUser))
    				        echo "<p>Your remaining balance is " . $currUser->getPoints() . " points."; 
    				?>
    				<form action="index.php">
    					<button type="submit" class="btn btn-warning">Keep Shopping</button>
    				</form>
    			</div>
    		</div>
		</div>
		
		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>	
		
		<!-- Scripts -->
		<script src="<?php echo Controller::resolvePath("js/weapons.js"); ?>"></script>
	</body>
</html>