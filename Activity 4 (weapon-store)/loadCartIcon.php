<?php 
    
    require_once('autoloader.php');
    use inc\data\ShoppingCartDao;
    use inc\web\WeaponStoreCache;
    use misd\security\SecurityService;
    use misd\web\Controller;

?>

<form action="cart.php" class="cart-container">
	<button type="submit">
		<img src="<?php echo Controller::resolvePath("res/img/cart.png"); ?>" alt="Shopping Cart" />
		<p>Shopping Cart</p>
		<span id="cart-item-count" class="badge badge-warning"><?php
		
			// insert logic to access the cart here
			$cart = null; $count = "";
			if (SecurityService::isCurrUserLoggedIn())
			{
			    // get an instance of the user
			    $user = SecurityService::getCurrentUser();
			    
			    // get the cart from the database...
			    $dao = new ShoppingCartDao();
			    $cart = $dao->getCartForUserId($user->getUserId());
			    if (!is_null($cart)) $count = count($cart);
			}
		    else 
		    {
		        // get the cart from session cache
		        $cart = WeaponStoreCache::get(WeaponStoreCache::SESSKEY_CART);
		        if (!is_null($cart)) $count = count($cart);
		    }
		    
		    // convert count to empty string if 0 or null
		    if (is_null($count) || $count == 0) $count = "";
		    
		    echo $count;
		    
		?></span>
	</button>
</form>