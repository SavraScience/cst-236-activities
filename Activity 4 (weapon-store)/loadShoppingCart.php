<?php

    // imports
    require_once('autoloader.php');
    use misd\web\Controller;
    use misd\security\SecurityService;
    use inc\data\ShoppingCartDao;
    use inc\web\WeaponStoreCache;
    use inc\models\ShoppingCartLineItemModel;
    use inc\data\WeaponDao;
    use inc\models\WeaponModel;

    // DECLARATIONS
    $cart = null;
    $userPoints = 0;
    
    // get resources
    $imgPathDelete = Controller::resolvePath("res/img/delete-32.png");
    
    if (SecurityService::isCurrUserLoggedIn())
    {
        // get some info about the user
        $user = SecurityService::getCurrentUser();
        $userPoints = $user->getPoints();
        
        // load shopping cart from database
        console_log("User is logged in!");
        console_log("Loading shopping cart from database...");
        $dao = new ShoppingCartDao();
        
        // WARNING! - You need a method to load all items by user's ID
        $cart = $dao->findAll();
        WeaponStoreCache::register(WeaponStoreCache::SESSKEY_CART, $cart);
    }
    else
    {
        // load in-memory shopping cart
        console_log("User is not currently logged in...");
        console_log("Loading shopping cart from session cache...");
        $cart = WeaponStoreCache::get(WeaponStoreCache::SESSKEY_CART);
    }
    
    /** @var $cart ShoppingCartLineItemModel[] */
    if (!is_null($cart))
    {
        // load weapon info
        $weapons = array();
        $dao = new WeaponDao();
        foreach ($cart as $cartItem)
        {
            /** @var $cartItem ShoppingCartLineItemModel */
            // inspect cart item info
            $weapon = $dao->findById($cartItem->getWeaponId());
            array_push($weapons, $weapon);
        }
        
        console_log("Loading cart into table...");
        
        // inspect cart info
        $numItems = count($cart);
        
        echo <<<ML
            <table id="shopping-cart-tbl" class="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Cost</th>

                        <!-- empty row header -->
                        <th></th>
                    <tr>
                </thead>
                <tbody>
ML;
        $cost;
        $totalCost = 0;
        for ($i = 0; $i < $numItems; $i++)
        {
            /** @var $weapons WeaponModel[] */
            /** @var $cartItem ShoppingCartLineItemModel */
            $cartItem = $cart[$i];
            
            // inspect cart item info
            $timid = $cartItem->getTempId();
            $weaponDesc = $weapons[$i]->getDescription();
            $cost = $weapons[$i]->getPointCost() * $cartItem->getQuantity();
            $totalCost += $cost;
            $qty = $cartItem->getQuantity();
            
            echo <<<ML
                <tr id="$timid">
                    <td>$weaponDesc</td>
                    <td>$qty</td>
                    <td>$cost points</td>
                    
                    <!-- empty row headers to account for buttons -->
                    <td align="center"><img src="$imgPathDelete" height="24" width="24" class="delete-btn" /></td>
                <tr>
ML;
        }
        
        // optional warning symbol & styling
        $optWarningMsg = "";
        $optWarningSym = "";
        $colorClass = "";
        
        if ($totalCost > $userPoints)
        {
            $optWarningMsg = ' title="You do not have sufficient funds to purchase!"';
            $optWarningSym = '<span class="badge badge-danger unselectable" style="margin-right: 9px;">!</span>';
            $colorClass = ' style="color: red;"';
        }
        
        echo <<<ML
                </tbody>
                <tfoot>
                    <tr>
                        <td><p>$numItems weapons</p></td>
                        <td><strong>Total Cost:</strong</td>
                        <td$colorClass><span$optWarningMsg>$optWarningSym<span id="cart-total-cost">$totalCost</span> points</span></td>
                    </tr>
                </tfoot>
            </table>
            <form action="checkout-handler.php">
                <button id="checkout-btn" type="button" class="btn btn-dark">Checkout</button>
            </form>
ML;
    }
?>
  <!-- Modals -->
  <div class="modal fade" id="modal-checkout">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 id="modal-title" class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <span id="modal-message"></span>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        	<button id="modal-btn-secondary" type="button" class="btn" data-dismiss="modal"></button>
        	<form action="checkout-handler.php">
          		<button id="modal-btn-primary" type="button" class="btn" data-dismiss="modal"></button>
          	</form>
        </div>
        
      </div>
    </div>
  </div>