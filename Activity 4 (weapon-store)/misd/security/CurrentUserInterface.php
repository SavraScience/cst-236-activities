<?php
namespace misd\security;

interface CurrentUserInterface
{
    function getUserId();
    function getUsername() : string;
    function getPoints() : int;
}

