<?php
namespace misd\security;

/**
 * A static class used to sanitize user input.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class InputSanitizer
{
    /**
     * 
     * Source: O'Reilly's Learning PHP, MySQL & JavaScript with jQuery, CSS & HTML5, 4th Ed.
     * by Robin Nixon
     * @copyright 2015 
     * @todo Add annotation properties for $connection param
     * @param $connection
     * @param string $string The string to be "fixed."
     * @return string
     */
    public static function mysql_entities_fix_string(\mysqli $connection, string $string)
    {
        return htmlentities(mysql_fix_string($connection, $string));
    }

    /**
     * 
     * Source: O'Reilly's Learning PHP, MySQL & JavaScript with jQuery, CSS & HTML5, 4th Ed.
     * by Robin Nixon
     * @copyright 2015
     * @todo Add annotation properties for $connection param
     * @param $connection
     * @param string $string The string to be "fixed."
     */
    private static function mysql_fix_string(\mysqli $connection, string $string)
    {
        if (get_magic_quotes_gpc()) $string = stripslashes($string);
        return $connection->real_escape_string($string);
    }
    
    /**
     * Sanitizes input from the user in a way that makes it safe to output back to the screen 
     * (HTML-safe).  Protects against XSS attacks.
     * 
     * Source: O'Reilly's Learning PHP, MySQL & JavaScript with jQuery, CSS & HTML5, 4th Ed.
     * by Robin Nixon
     * @copyright 2015 
     * @param string $input The input from the user to be sanitized.
     * @return string The sanitized, HTML-safe version of the user's input.
     */
    public static function sanitizeString(string $input)
    {
        $input = stripslashes($input);
        $input = strip_tags($input);
        $input = htmlentities($input);
        return $input;
    }

    /**
     * Sanitizes all input in an array, such as the values passed into
     * the global $_POST array.  The function does not return anything
     * because the array is passed in and processed By Reference.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param array $array The array (of strings) to be sanitized. This will
     * most likely be a $_POST array, such as when processing a form.
     * @return void
     */
    public static function sanitizeStringArray(array &$array)
    {
        foreach($array as &$input)
        {
            $input = self::sanitizeString($input);
        }
    }
    
    /**
     * Sanitizes input from the user in a way that makes it safe to store data (SQL-safe).
     * Protects against SQL injection AS WELL AS XSS attacks, as it uses the 
     * sanitizeString() function within its code.
     *
     * Source: O'Reilly's Learning PHP, MySQL & JavaScript with jQuery, CSS & HTML5, 4th Ed.
     * by Robin Nixon
     * @copyright 2015
     * @todo Add annotation properties for $connection param
     * @param $connection
     * @param string $input The input from the user to be sanitized.
     * @return string The sanitized, SQL-safe version of the user's input.
     */
    public static function sanitizeMySQL(\mysqli $connection, string $input)
    {
        $input = $connection->real_escape_string($input);
        $input = self::sanitizeString($input);
        return $input;
    }
}

