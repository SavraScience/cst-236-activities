<?php
namespace misd\web;

/**
 * A generic, static controller class to handle redirects and basic path resolving for files
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class Controller
{
    // PUBLIC CONSTANTS
    public const APP_PATH = 'APP_PATH';
    public const APP_PARTIAL = 'APP_PARTIAL';
    public const CURR_PAGE_ID = 'CURR_PAGE_ID';
    public const CURR_PAGE_TITLE = 'CURR_PAGE_TITLE';
    
    /**
     * Returns the full application path as defined in the environmental global
     * @author Michael Mason <misdllc@gmail.com>
     * @copyright 2019 Mason Innovative Software Design
     * @return string|null The full path of the application, including the server's domain
     */
    public static function appPath()
    {
        if (!isset($_ENV[self::APP_PATH]))
            $_ENV[self::APP_PATH] = $_SERVER['DOCUMENT_ROOT'] . self::appPartialUri();
        return $_ENV[self::APP_PATH];
    }
    
    /**
     * Returns only the application path relative to the server's root path
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string|null The root path of the application relative to the server's domain
     */
    public static function appPartialUri() : string
    {
        if (!isset($_ENV[self::APP_PARTIAL]))
            self::setAppPartialUri("/weapon-store/");
        return $_ENV[self::APP_PARTIAL];
    }
    
    /**
     * Allows any subclass to set the current PHP application's
     * subpath
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $appPath The (sub)path of your PHP application
     */
    protected static function setAppPartialUri(string $appPath) : void
    {
        $_ENV[self::APP_PARTIAL] = $appPath;
    }
    
    /**
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $page A string representation of the page desired to be resolved.
     */
    public static function redirect(string $page)
    {
        $path = self::resolvePath($page);
        console_log("Path resolved to '$path'...");
        header("Location: $path");
    }
    
    /**
     * Resolves the path of a given page, which should be the path and filename of the page, minus the
     * web application's root folder and the file extension of the desired page to be resolved.  If
     * an extension is included with the filename, that extension will automatically be used.
     * 
     * @example If the domain is "localhost/" and the application's root folder is "AppName/",
     * and the $page argument is "index", the path would be resolved to:
     * "localhost/AppName/index.php"
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $file A string representation of the page desired to be resolved.
     * @return string The full path of the page as resolved
     */
    public static function resolvePath(string $file)
    {
        // TODO: Come back and revisit to clean up
        // supported file extensions
        // note: simply add more file extensions as needed
        $fileExtensions = array('.php', '.js', '.css');
        $fileInfo = pathinfo($file);        
        $partialPath = self::appPartialUri() . $file;

        if (!isset($fileInfo['extension']))
        {
            //console_log('The parameter $file has no extension...');
            foreach ($fileExtensions as $ext)
            {
                $pathAndExtension = $partialPath . $ext;
                //console_log("Attempting to resolve '$pathAndExtension'");
                $fullPath = self::appPath() . $file . $ext;
                //console_log("Full path: '$fullPath'");
                $fullPath = switchSlashes(realpath($fullPath));
                //console_log("Looking for file @ '$fullPath'");
                if (file_exists($fullPath)) 
                {
                    //console_log("Path resolved to '$path'");
                    return $pathAndExtension;
                }
            }
        }
        else 
        {
            //console_log("Partial path = '$partialPath'");
            return $partialPath;
        }
        
        //$path = self::appPartialUri() . "$page.php";
        //console_log("Could not resolve path...");
        return "";
    }
    
    /**
     * A wrapper to PHP's include function, which prepends
     * the application's full path as defined in an external 
     * config file
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $pageUri
     */
    public static function include(string $pageUri) : void
    {
        include self::appPath() . $pageUri;
    }
    
    /**
     * A wrapper to PHP's include_once function, which prepends
     * the application's full path as defined in an external config file
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $pageUri
     */
    public static function includeOnce(string $pageUri) : void
    {
        include_once self::appPath() . $pageUri;
    }
    
    /**
     * A wrapper to PHP's require function, which prepends
     * the application's full path as defined in an external
     * config file
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $pageUri
     */
    public static function require(string $pageUri) : void
    {
        require self::appPath() . $pageUri;
    }
    
    /**
     * A wrapper to PHP's require_once function, which prepends
     * the application's full path as defined in an external config file
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $pageUri
     */
    public static function requireOnce(string $pageUri) : void
    {
        require_once self::appPath() . $pageUri;
    }
}

