<?php
namespace misd\web;

use misd\models\AbstractObjectModel;

/**
 * A wrapper class for the $_SESSION[] global with
 * additional functionality
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class SessionCache
{
    // CONSTANTS
    private const CACHE_KEY = "cache";
    
    public static function persistSession() : void
    {
        if (session_status() == PHP_SESSION_NONE)
        {
            //console_log("Session was not previously active...");
            session_start();
        }
    }
    
    /**
     * Stores a variable inside the $_SESSION global
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $key The key for the session variable
     * you want to use to store your value
     * @param mixed $value Any scalar value, object, or array
     * you want to store using the given $key
     */
    public static function register(string $key, $value) : void
    {
        if (session_status() == PHP_SESSION_NONE)
        {
            session_start();
            
            // initialize the cache
            //console_log("Initializing session cache...");
            $_SESSION[self::CACHE_KEY] = array();
        }
        
        $_SESSION[self::CACHE_KEY][$key] = $value;
        //console_log(gettype($value) . ((!is_array($value)) ? " " . $value : "") . " registered at session key '$key'");
        
        // suppress notices about string to array conversion
        //$level = error_reporting();
        //console_log("Error Level was $level");
        //error_reporting(0);
        
        // debugging
        //console_log('$_SESSION[\'' . self::CACHE_KEY . "']['$key'] = " . $_SESSION[self::CACHE_KEY][$key]);
        
        // restore previous error reporting level
        //error_reporting($level);
    }
        
    /**
     * Retrieves a value from the $_SESSION global, if
     * one exists at the given key; otherwise, returns
     * null
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $key The key in which the expected
     * variable (value) is stored.
     * @return mixed|NULL
     */
    public static function get(string $key)
    {
        self::persistSession();
        if (session_status() == PHP_SESSION_ACTIVE)
        {
            if (isset($_SESSION[self::CACHE_KEY][$key]))
            {
                //console_log("Variable found in session cache at key '$key'.");
                return $_SESSION[self::CACHE_KEY][$key];
            }
            //console_log("Variable could NOT be found in session cache at key '$key'");
        }
        else
        {
            // session not active
            //console_log("Session not currently active (status code = " . session_status() . ')');
        }
        return null;
    }
    
    /**
     * Clears either the entire $_SESSION cache (using 
     * the cache key) if no key is provided OR clears
     * only the cache for a specific key
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $key (Optional) The key you want to
     * clear in the cache
     */
    public static function clear(string $key = "") : void
    {
        if (empty($key))
        {
            // clear the entire session cache
            unset($_SESSION[self::CACHE_KEY]);
        }
        else
        {
            // clear the cache for a specific key
            unset($_SESSION[self::CACHE_KEY][$key]);
        }
    }

    /**
     * A debugging method
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $key
     * @param mixed $var
     * @return void
     */
    public static function printKeyVal(string $key) : void
    {
        // first, check the cache to see if any variable is
        // stored at the given key location
        
        // debugging
        echo "Checking session cache for key '$key'...<br />\n";
        
        $keyVal = self::get($key);
        if (!empty($keyVal))
        {
            echo "Value found in session cache at key '$key'...<br />\n";
            if (is_array($keyVal))
            {
                echo "Value is array.\n";
                $array = $keyVal;
                
                // debugging
                $i = 0;
                echo "Array has " . count($array) . " items...<br />\n";
                
                foreach ($array as $val)
                {
                    // debugging
                    $tmp = "";
                    if (stringConvertable($val)) 
                        $tmp = " = $val";
                    else
                        $tmp = " = " . get_class($val);
                    
                    echo "'$key'[$i]" . $tmp;
                    if ($val instanceof AbstractObjectModel)
                    {
                        /** @var $val AbstractObjectModel */
                        echo " || Temp ID = " . $val->getTempId() . " || Real ID = " . $val->getId() . "<br />\n";
                    }
                    $i++;
                }
            }
            else
            {
                echo "Value is a single variable of type " . get_class($keyVal) . "<br />\n";
                if (stringConvertable($keyVal)) echo "Value = $keyVal<br />\n";
                echo $keyVal;
            }
        }
        echo "<br />";
    }
    
    public static function remove(string $key, $var) : bool
    {
        // first, check the cache to see if any variable is
        // stored at the given key location
        
        // debugging
        $variable = (stringConvertable($var)) ? "'" . $var . "'" : 'Variable';
        //if (stringConvertable($var)) console_log("Looking for value '$var' in session cache at key '$key'");
        
        $keyVal = self::get($key);
        if (!empty($keyVal))
        {
            if (is_array($keyVal))
            {
                $array = &$keyVal;
                
                // debugging
                $i = 0;
                //console_log("Cache value at key '$key' is an array with " . count($array) . " items...");
                
                foreach ($array as $val)
                {
                    // debugging
                    $tmp = "";
                    if (stringConvertable($val)) $tmp = " = $val";
                    //console_log("Checking '$key'[$i]" . $tmp);
                    
                    if (self::varValCompare($var, $val))
                    {
                        // debugging
                        if (stringConvertable($val)) $variable = $val;
                        //console_log("$variable resolved in session cache array at key '$key'.");
                        //console_log("Removing element at array index #$i");
                        
                        // remove element and re-index
                        unset($array[$i]);
                        $array = array_values($array);
                        
                        return true;
                    }
                    $i++;
                }
            }
            else
            {
                if (self::varValCompare($var, $keyVal))
                {
                    if (stringConvertable($val)) $variable = $val;
                    //console_log("$variable resolved in session cache at key '$key'.");
                    $keyVal = null;
                    return true;
                }
            }
        }
        
        // variable could not be resolved -- return self
        //console_log("$variable could not be resolved in session cache at key '$key'. Returning itself...");
        return false;
    }
    
    public static function resolve(string $key, $var)
    {
        // first, check the cache to see if any variable is
        // stored at the given key location
        
        // debugging
        $variable = (stringConvertable($var)) ? "'" . $var . "'" : 'Variable';
        //if (stringConvertable($var)) console_log("Looking for value '$var' in session cache at key '$key'");
        
        $keyVal = self::get($key);
        if (!empty($keyVal))
        {
            if (is_array($keyVal))
            {
                $array = $keyVal;
                
                // debugging
                $i = 0;
                //console_log("Cache value at key '$key' is an array with " . count($array) . " items...");
                
                foreach ($array as $val)
                {
                    // debugging
                    $tmp = "";
                    if (stringConvertable($val)) $tmp = " = $val";
                    //console_log("Checking '$key'[$i]" . $tmp);
                    
                    if (self::varValCompare($var, $val)) 
                    {
                        // debugging
                        if (stringConvertable($val)) $variable = $val;
                        //console_log("$variable resolved in session cache array at key '$key'.");
                        return $val;
                    }
                    $i++;
                }
            }
            else
            {
                if (self::varValCompare($var, $keyVal)) 
                {
                    if (stringConvertable($val)) $variable = $val;
                    //console_log("$variable resolved in session cache at key '$key'.");
                    return $keyVal;
                }
            }
        }
        
        // variable could not be resolved -- return self
        //console_log("$variable could not be resolved in session cache at key '$key'. Returning itself...");
        return $var;
    }
    
    private static function varValCompare($varToResolve, $val)
    {
        if (is_object($val) && $val instanceof AbstractObjectModel)
        {
            // compare using specialized comparison
            /** @var $varToResolve AbstractObjectModel */
            //console_log("Comparing using AbstractObjectModel::compare() method");
            return ($varToResolve::compare($varToResolve, $val));
        }
        else
        {
            // try using standard comparison
            //console_log("Comparing using standard comparison techniques (==)");
            return ($varToResolve == $val);
        }
    }
}

