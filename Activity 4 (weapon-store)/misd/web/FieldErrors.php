<?php
namespace misd\web;

/**
 * A class that encapsulates form field errors
 * by keeping track of a field's name, original
 * value, and the error(s) associated with it.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class FieldErrors
{
    // INSTANCE VARIABLES
    private $formFieldName;
    private $formFieldValue;
    private $fieldErrors;
    
    // CONSTRUCTOR
    public function __construct(string $fieldName, $fieldValue)
    {
        $this->formFieldName = $fieldName;
        $this->formFieldValue = $fieldValue;
        
        // initialize $fieldErrors
        $this->fieldErrors = array();
    }  
    
    // ACCESSOR METHODS
    /**
     * Returns the name of the field for which the error
     * was found
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string The name of the field for which
     * the error is bound.
     */
    public function getFieldName() : string
    {
        return $this->formFieldName;
    }

    /**
     * Returns the original field value which caused
     * the error
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return mixed The original value of the form field
     * which caused the error to trigger
     */
    public function getFieldValue()
    {
        return $this->formFieldValue;
    }  
    
    /**
     * Returns an array of error descriptions
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return array
     */
    public function getErrors() : array
    {
        return $this->fieldErrors;
    }
    
    // PUBLIC METHODS
    /**
     * Adds a new error for the associated form field
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $errorMsg
     */
    public function addError(string $errorMsg) : void
    {
        if (!$this->hasError($errorMsg)) 
            array_push($this->fieldErrors, $errorMsg);
    }
    
    /**
     * Clears all errors for the associated form field
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function clear() : void
    {
        $this->fieldErrors = array();
    }
    
    /**
     * Checks to see if an error with the same description
     * has already been logged for the associated form field
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $errorDesc
     * @return boolean
     */
    public function hasError(string $errorDesc)
    {
        foreach ($this->fieldErrors as $error)
        {
            if ($error == $errorDesc)
                return true;
        }
        return false;
    }
    
    /**
     * Returns true if the associated form field has
     * errors associated with it; otherwise, returns false
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return bool
     */
    public function hasErrors() : bool
    {
        if (count($this->fieldErrors) > 0)
            return true;
        return false;
    }
}

