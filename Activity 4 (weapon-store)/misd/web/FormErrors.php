<?php
namespace misd\web;

/**
 * A class that encapsulates all form field errors
 * for an entire form.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class FormErrors
{
    // CONSTANTS
    
    // INSTANCE VARIABLES
    /** @var $errorsList FieldErrors[] */
    private $errorsList;
    
    // CONSTRUCTOR
    public function __construct() 
    {
        $this->errorsList = array();
    }
    
    /**
     * Adds an error for a given form field
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $fieldName The form field name for
     * which an error was found
     * @param mixed $fieldValue The original field value
     * that triggered the error
     * @param string $errorDesc A description of the error
     * that corresponds to the original form field's value
     */
    public function addError(string $fieldName, $fieldValue, string $errorDesc) : void
    {
        // first, check to see if the given field already exists in the array
        $fieldError = null;   
        if (array_key_exists($fieldName, $this->errorsList))
            $fieldError = $this->errorsList[$fieldName];
        
        // if errors for field did not currently exist, create one
        if (is_null($fieldError)) $fieldError = new FieldErrors($fieldName, $fieldValue);
        
        // next, add the given error if it does not already exist in the list of errors
        $fieldError->addError($errorDesc);
        
        // finally, add (or re-add) the error back to master errors list
        $this->errorsList[$fieldName] = $fieldError;
    }
    
    /**
     * Returns a FieldErrors object for a given field
     * if one exists; otherwise, returns null.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $fieldName The name of the field
     * for which you want to return a FieldErrors object for
     * @return \misd\web\FieldErrors|NULL
     */
    public function getErrorsForField(string $fieldName)
    {
        if (array_key_exists($fieldName, $this->errorsList))
            return $this->errorsList[$fieldName];
        return null;
    }
    
    /**
     * Indicates whether or not the form has any associated
     * errors with it
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return bool
     */
    public function hasErrors() : bool
    {
        if (count($this->errorsList) > 0)
            return true;
        return false;
    }
    
    public function validate($object, \ReflectionMethod $getter) : void
    {
        
    }
}

