<?php
namespace misd\models;

/**
 * An abstract object model that all other model
 * objects can extend from.  By extending from this
 * class, developers get the added benefit of instantiating
 * from any DAO class that derives from AbstractDao, deleting
 * and saving changes to the database more quickly using the
 * AbstractObjectModel's corresponding database ID.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
abstract class AbstractObjectModel
{
    // INSTANCE VARIABLES
    protected $id;
    protected $tempId;
    
    // CONSTRUCTOR
    /**
     * An integer value can be passed in when instantiating
     * this model from an existing record in the database
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param int $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
        $this->tempId = uniqid();
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return int The corresponding database ID to this
     * object model
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Returns an in-memory ID that acts as a proxy for
     * identification as an alternative to the in-memory
     * object's database ID, which can be compromised
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string
     */
    public function getTempId() : string
    {
        return $this->tempId;
    }
    
    /**
     * Enables you to set a temporary, in-memory ID that
     * can be resolved against an object already existing
     * in session cache
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tempId
     */
    public function setTempId(string $tempId) : void
    {
        $this->tempId = $tempId;
    }
    
    /**
     * Provides a way to compare two AbstractObjectModels
     * using in-memory identity AND/OR referential identity
     * (using each object's database $id field)
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param AbstractObjectModel $obj1 First object to use in comparison
     * @param AbstractObjectModel $obj2 Second object to use in comparison
     * @return boolean Indicates whether or not the two objects are identical
     */
    public static function compare(AbstractObjectModel $obj1, AbstractObjectModel $obj2) : bool
    {
        // debugging
        //console_log("Comparing using AbstractObjectModel::compare()...");
        //console_log('$obj1->id = ' . $obj1->id . ', $obj2->id = ' . $obj2->id);
        //if (stringConvertable($obj1) && stringConvertable($obj2))
            //console_log("Comparing '$obj1' to '$obj2'");
        
        if ($obj1 === $obj2)
        {
            //console_log('$obj1 is exactly identical in-memory to $obj2');
            //console_log("AbstractObjectModel::compare() succeeded...");
            return true;
        }
        elseif ($obj1->id > 0 && $obj2->id > 0)
        {
            if ($obj1->id === $obj2->id) 
            {
                //console_log("\$obj1->id ($obj1->id) === \$obj2->id ($obj2->id)");
                //console_log("AbstractObjectModel::compare() succeeded...");
                return true;
            }
        }
        
        // next, check to see if their temporarily assigned IDs are the same
        //console_log("Comparing \$obj1->tempId = $obj1->tempId to \$obj2->tempId = $obj2->tempId");
        if ($obj1->tempId === $obj2->tempId)
        {
            //console_log("\$obj1->tempId ($obj1->tempId) === \$obj2->tempId ($obj2->tempId)");
            //console_log("AbstractObjectModel::compare() succeeded...");
            return true;
        }
        
        //console_log("AbstractObjectModel::compare() failed...");
        return false;
    }
}

