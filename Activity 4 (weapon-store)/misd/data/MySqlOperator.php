<?php
namespace misd\data;

require_once ('ComparisonOperatorInterface.php');

/**
 * A MySQL set of operators accessible via factory methods
 * which act as string enumerations
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class MySqlOperator implements ComparisonOperatorInterface
{
    // INSTANCE VARIABLES
    private $value;
    
    // CONSTRUCTOR
    
    /**
     * A private constructor that allows the class's
     * static functions to instantiate itself using
     * the correct string for its value property
     * (the operator)
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $stringOperator The read-only
     * operator value that will be stored with the object
     */
    private function __construct(string $stringOperator)
    {
        $this->value = $stringOperator;
    }
    
    // PUBLIC METHODS
    
    /**
     * A string representing the comparison operator stored
     * within the object
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string
     */
    public function value() : string
    {
        return $this->value;
    }
    
    // STATIC FUNCTIONS
    
    /**
     * Returns an equals ("=") sign as stored in the MySqlOperator object
     * {@inheritDoc}
     * @see \misd\data\ComparisonOperatorInterface::EQUALS()
     */
    public static function EQUALS()
    {
        return new MySqlOperator("=");
    }
    
    /**
     * Returns an greater-than (">") sign as stored in the MySqlOperator object
     * {@inheritDoc}
     * @see \misd\data\ComparisonOperatorInterface::GREATER_THAN()
     */
    public static function GREATER_THAN()
    {
        return new MySqlOperator(">");
    }
    
    /**
     * Returns a greater-than-or-equal-to (">=") sign as stored in the MySqlOperator object
     * {@inheritDoc}
     * @see \misd\data\ComparisonOperatorInterface::GREATER_THAN_OR_EQUALS()
     */
    public static function GREATER_THAN_OR_EQUALS()
    {
        return new MySqlOperator(">=");
    }
    
    /**
     * Returns a less-than ("<") sign as stored in the MySqlOperator object
     * {@inheritDoc}
     * @see \misd\data\ComparisonOperatorInterface::LESS_THAN()
     */
    public static function LESS_THAN()
    {
        return new MySqlOperator("<");
    }
    
    /**
     * Returns a less-than-or-equal-to ("<=") sign as stored in the MySqlOperator object
     * {@inheritDoc}
     * @see \misd\data\ComparisonOperatorInterface::LESS_THAN_OR_EQUALS()
     */
    public static function LESS_THAN_OR_EQUALS()
    {
        return new MySqlOperator("<=");
    }
    
    /**
     * Returns a LIKE ("LIKE") sign used in finding partial matches
     * as stored in the MySqlOperator object
     * {@inheritDoc}
     * @see \misd\data\ComparisonOperatorInterface::LIKE()
     */
    public static function LIKE()
    {
        return new MySqlOperator("LIKE");
    }
    
    /**
     * Returns a wildcard symbol ("%") as stored in the MySqlOperator object
     * {@inheritDoc}
     * @see \misd\data\ComparisonOperatorInterface::WILDCARD()
     */
    public static function WILDCARD()
    {
        return new MySqlOperator("%");
    }
    
    public function __toString() : string
    {
        return $this->value;
    }
}

