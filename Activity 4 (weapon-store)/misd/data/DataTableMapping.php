<?php
namespace misd\data;

use misd\models\AbstractObjectModel;

/**
 * A class to store mappings between model properties
 * and table field names.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class DataTableMapping
{
    // INSTANCE VARIABLES
    private $classProperty;
    private $tableName;
    private $fieldName;
    private $mapFkToDescriptor;
    
    /** @var $dao Dao */
    private $dao;
    
    // CONSTRUCTOR
    
    /**
     * Provides a way to map the properties of a class to a database 
     * table's fields
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $classPropertyName The name of the property you
     * want to map to a table field
     * @param string $tableFieldName The name of the table field you
     * are mapping to a class property
     * @param string $tableName The name of the table that contains
     * the field you want to match (optional)
     * @param bool $resolveFkToDescriptor (Optional) When this value
     * is true, the DAO will attempt to resolve a foreign key to the
     * first CHAR/VARCHAR in the foreign table it relates to.  This
     * is the default behavior.  If you are simply mapping a foreign
     * key value directly to the class property as an integer, you
     * must specify that this value be false
     * @param Dao $dao (Optional) The DAO you want to use to resolve
     * this property to an object using another table
     */
    public function __construct(
        string $classPropertyName, 
        string $tableFieldName, 
        string $tableName,
        bool $resolveFkToDescriptor = true,
        Dao $dao = null
    )
    {
        if (!empty($classPropertyName)) $this->classProperty = $classPropertyName;
        if (!empty($tableName)) $this->tableName = $tableName;
        if (!empty($tableFieldName)) $this->fieldName = $tableFieldName;
        $this->mapFkToDescriptor = $resolveFkToDescriptor;
        if (!is_null($dao)) $this->dao = $dao;
    }
    
    // ACCESSOR METHODS
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string The class's property, private or public,
     * to be matched with its corresponding table field
     */
    public function getClassProperty() : string
    {
        // debugging
        //console_log("Accessing class property '$this->classProperty'");
        return $this->classProperty;
    }

    /**
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return Dao|null The DAO attached to this 
     * DataTableMapping, if any
     */
    public function getDao()
    {
        return $this->dao;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string The name of the table housing the 
     * field to be associated
     */
    public function getTableName() : string
    {
        return $this->tableName;
    }

    /** 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string The name of the table field to be
     * associated with the class property     
     */
    public function getFieldName() : string
    {
        return $this->fieldName;
    }
    
    public function mapKeyToDescriptor() : bool
    {
        return $this->mapFkToDescriptor;
    }
    
    /**
     * Prints the DataTableMapping as "Class property {class property}
     * maps to table field {tableName}.{fieldName}
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string
     */
    public function __toString() : string
    {
        return "Class property '$this->classProperty' maps to table field '" . 
            (empty($this->tableName) ? '' : $this->tableName . '.') . "$this->fieldName'";
    }
}

