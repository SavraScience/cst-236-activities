<?php
namespace misd\data;

/**
 * An interface that forces classes to
 * implement comparison operators for any programming
 * language.  Method names are all upper case to 
 * indicate that implementing classes are to be treated 
 * like an enumerator.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
interface ComparisonOperatorInterface
{    
    // PUBLIC METHODS
    public function value();
    
    // STATIC METHODS
    public static function EQUALS();
    public static function GREATER_THAN();
    public static function GREATER_THAN_OR_EQUALS();
    public static function LESS_THAN();
    public static function LESS_THAN_OR_EQUALS();
    public static function LIKE();
    public static function WILDCARD();
}

