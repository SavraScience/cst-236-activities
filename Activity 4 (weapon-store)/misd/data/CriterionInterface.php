<?php
namespace misd\data;


interface CriterionInterface
{
    /**
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string
     */
    public function getPropertyName() : string;
    
    /**
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return ComparisonOperatorInterface
     */
    public function getOperator() : string;
    
    /**
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return mixed
     */ 
    public function getCriterionValue();
    
    /**
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function setCriterionValue($critVal) : void;
    
    /**
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return CriterionTypeInterface
     */
    public function getCriterionType() : CriterionTypeInterface;
    
    /**
     * Checks to see if the this Criterion is equal to its argument
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param CriterionInterface $criterion The Criterion you want to
     * compare to
     * @return bool
     */
    public function equals(self $criterion) : bool;
    
    /**
     * Removes (this) criterion from its associated
     * QueryObject
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function remove() : void;
}

