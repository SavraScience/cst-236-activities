<?php
namespace misd\data;

/**
 * An interface for all DAOs to extend from
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
interface DaoInterface
{
    /**
     * Returns all records from the database
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @TODO: Update return value when you've decided what you
     * want to return here
     */
    public function findAll();
    
    /**
     * Returns a single record from the database using a given ID
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param int $id The integer/long ID value of the database record 
     * you want to retrieve
     * @TODO: Update return value when you've decided what you
     * want to return here
     */
    public function findById($id);

    /**
     * Creates a new record in the database using
     * a model or domain object
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param object $object
     * @return int The ID of the newly inserted value.
     * If this value returns 0, then the database
     * insertion was not successful
     */
    public function insert($object) : int;
    
    /**
     * Updates a single record from the database using
     * a model or domain object
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param object $object
     * @return bool Indicates if the row update was 
     * successful (true) or not (false)
     */
    public function update($object) : bool;
    
    /**
     * Deletes a single record from the database using a given ID
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param int $id
     * @return bool Indicates if the row was 
     * successfully deleted (true) or not (false)
     */
    public function deleteById($id) : bool;
 
    /**
     * Attempts to locate one or many rows in the database
     * based upon a single object instance or a query object
     * that can be used to specify some criteria
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param object $object A model object or query object
     */
    public function find($object);
    
    /**
     * Attempts to resolve the primary key for a record matching
     * the unique descriptor value ($desciption) passed in as an
     * argument using the $descriptorField
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $descriptorField The field you want to use
     * to match the $description 
     * @param string $description A unique value, usually a candidate
     * key for a small table with just a few fields, that can be queried
     * to retrieve a single row in the table
     * @param bool $exactMatchOnly Indicates whether or not to return an
     * exact match, or a list of LIKE matches from the query result
     */
    public function findIdViaDescriptor(string $descriptorField, string $description, bool $exactMatchOnly = true);
    
}

