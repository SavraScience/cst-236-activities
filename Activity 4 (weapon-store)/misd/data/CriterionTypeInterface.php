<?php
namespace misd\data;

/**
 * Specifies a criterion type, either AND or OR, for
 * a QueryObject.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
interface CriterionTypeInterface
{
    // PUBLIC CONSTANTS
    public const AND = 0;
    public const OR = 1;
    
    // PUBLIC METHDOS
    public function value() : string;
    
    // STATIC METHODS
    public static function AND() : CriterionTypeInterface;
    public static function OR() : CriterionTypeInterface;
}

