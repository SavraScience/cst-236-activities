<?php

    // imports
    require_once 'autoloader.php';
    use misd\security\SecurityService;
    use misd\web\Controller;
    use inc\data\ShoppingCartDao;
    use inc\models\ShoppingCartLineItemModel;
    use inc\data\WeaponDao;
use misd\web\SessionCache;
use inc\data\UserDao;
use inc\models\UserModel;
use inc\web\WeaponStoreCache;

    // get the current user
    SessionCache::persistSession();
    $user = SecurityService::getCurrentUser();   
    if (!is_null($user))
    {
        // debugging
        //console_log('$user is a ' . get_class($user));
        
        $userPoints = $user->getPoints();
        
        // get the shopping cart for current user
        /** @var $cart ShoppingCartLineItemModel[] */
        $dao = new ShoppingCartDao();
        $cart = $dao->getCartForUserId($user->getUserId());
        
        // tally up total points
        $dao = new WeaponDao();
        $cartTotal = 0;
        foreach ($cart as $item)
        {
            /** @var $item ShoppingCartLineItemModel */
            
            // resolve weapon
            $weapon = $dao->findById($item->getWeaponId());
            
            if (!is_null($weapon))
            {
                /** @var $weapon \inc\models\WeaponModel */
                
                // add point cost
                $cost = $weapon->getPointCost();
                $cartTotal += $item->getQuantity() * $cost;
            }
        }
        
        // check to see if the user has enough points
        if ($userPoints >= $cartTotal)
        {
            // user can proceed with checkout
            // -- get the user, alter points, and update the database
            $dao = new UserDao();
            $user = $dao->findById($user->getUserId());
            
            /** @var $user UserModel */
            $pointDiff = $userPoints - $cartTotal;
            console_log("Setting user's new points to $pointDiff...");
            $user->setPoints($pointDiff);
            $result = $dao->update($user);
            
            if ($result)
            {
                // WARNING: NOT A CONCURRENT APPROACH!
                // -- update shopping cart to reflect purchase
                $dao = new ShoppingCartDao();
                $result = $dao->deleteCartForUserId($user->getId());
                
                if ($result)
                {
                    // refresh cache
                    SecurityService::refreshPrincipal();
                    // redirect to order confirmation page
                    Controller::redirect("order-confirmation.php");
                }
            }
        }
        else
        {
            // user does not have enough poitns to checkout
            console_log("User does not have sufficient points...");
        }
    }
    else
    {
        // something went wrong
        console_log("Something went wrong...");
    }
?>