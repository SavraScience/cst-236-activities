"use strict";

// DECLARATIONS
//var addToCartButtons = document.querySelectorAll(".add-to-cart-btn");
var addToCartButtons = document.getElementsByClassName("add-to-cart-btn");
//console.log("JavaScript doc should have loaded...");

// EVENT HANDLERS
function addToCart(btn)
{
	// get the hidden value of the button clicked
	var form = btn.closest("form");
	var uri = form.action;
	var timid = form.elements[0].value;

	// debugging
	console.log("You clicked button id #" + timid + "!");
	console.log("URI is '" + uri + "'");

	// asynchronous requests
	var data = { timid: timid };
	$.post(uri, data, function(response) {

			// debugging
			console.log(response);

			if (response == 1)
			{
				// update the cart count
				var spanCount = document.getElementById("cart-item-count");
				var currCount = spanCount.innerText;
				if (currCount == "")
				{
					currCount = 0;
				}
				spanCount.innerText = ++currCount;

				// change card & button appearance
				btn.innerText = "In Cart";
				btn.className = "btn btn-warning add-to-cart-btn";
				btn.setAttribute("disabled", "disabled");
				var card = btn.closest(".card");
				card.classList.add('in-cart');
			}
			else if (response == 0)
			{
				// display a message
				console.log("Failure");
			}		
	});
}

/* Attach Events */
Array.prototype.forEach.call(addToCartButtons, function(item) {
	item.addEventListener('click', function() {
		addToCart(this);
	}, false);
});

// helper function
function dump(obj)
{
	var out = '';
	for (var i in obj)
	{
		out += i + ": " + obj[i] + "\n";
	}
	console.log(out);
}