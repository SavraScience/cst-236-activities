"use strict";

$(document).ready(documentOnLoad());
function documentOnLoad() 
{
    // show delete button on hover
    $('#shopping-cart-tbl tbody tr').on('mouseover', function() {

      
      // build selector query
      var select = '#' + this.id + ' img.delete-btn'; 

      // get image element
      var img = $(select);

      if (img.length != 0)
      {
        //console.log('Showing image at row #' + this.id);
        img.css('opacity', '1');
      }
    });

    // hide delete button on hover
    $('#shopping-cart-tbl tbody tr').on('mouseout', function() {

      // build selector query
      var select = '#' + this.id + ' img.delete-btn'; 

      // get image element
      var img = $(select);

      if (img.length != 0)
      {
        //console.log('Hiding image at row #' + this.id);
        img.css('opacity', '0.33');
      }

    } );

    // delete button 'click' handler
    $('img.delete-btn').on('click', function() {

      // get the row element
      var row = this.closest('tr');

      if (row.length != 0)
      {
        //console.log("You clicked delete for row #" + row.id);

        // send asyncronous request to the server to delete the selected row
        $.post('deleteShoppingCartItem-handler.php', { timid: row.id }, function(data) {
          if (data == 0)
          {
            //console.log('Could not delete!');

          }
          else
          {
            // reload shopping cart
            $('#shopping-cart').html(data);

            // update cart count
            var cartCountEl = $('#cart-item-count');
            var cartCount = parseInt(cartCountEl.text());
            cartCount--;

            if (cartCount == 0)
              cartCountEl.css('visibility', 'hidden');
            else
              cartCountEl.text(cartCount);

            // reload document
            documentOnLoad();
          }

        });
      }

    });

    // checkout button 'click' handler
    $('#checkout-btn').on('click', function() {

      // get user points
      var userPointsEl = $('#user-points');
      if (userPointsEl.length > 0)
      {
        var userPoints = parseInt(userPointsEl.text(), 10);
        console.log("User points: " + userPoints); 

        // get total checkout points
        var cartTotalEl = $('#cart-total-cost');
        if (cartTotalEl.length > 0)
        {
          var cartTotalPoints = parseInt(cartTotalEl.text());
          console.log("Total cart cost: " + cartTotalPoints);
          
          // get modal elements
          var modalHeader = $('#modal-title');
          var modalMessage = $('#modal-message');
          var modalPrimaryBtn = $('#modal-btn-primary');
          var modalSecondaryBtn = $('#modal-btn-secondary');

          if (userPoints >= cartTotalPoints)
          {
            // user has enough points to check out
            // -- style modal
            // ---- header ----
            modalHeader.text("Confirm Checkout");
            modalHeader.attr('class', 'modal-title');
            // ---- message
            modalMessage.text("Are you sure you want to check out?  Doing so will " +
              "deduct the balance of your shopping cart from your current account balance!");
            // ---- buttons ----
            modalPrimaryBtn.html("<u>Y</u>es");
            modalPrimaryBtn.attr('class', 'btn btn-warning');
            modalPrimaryBtn.attr('accesskey', 'y');
            // ---- show secondary button
            modalSecondaryBtn.html("<u>N</u>o");
            modalSecondaryBtn.attr('class', 'btn btn-secondary');
            modalSecondaryBtn.show();
            modalSecondaryBtn.attr('accesskey', 'n');

            // change primary button onClick() behavior
            modalPrimaryBtn.on('click', function() {

              // get closest form and submit 
              var form = modalPrimaryBtn.closest('form');
              console.log('Navigating to ' + form.attr('action'));
              form.submit();

            });
          }
          else
          {
            // insufficient funds!
            // -- style modal
            // *-- header --*
            modalHeader.text("Cannot checkout!");
            modalHeader.attr('class', 'modal-title text-danger');
            // *-- message --*
            modalMessage.text("You do not have enough points to purchase these weapons! Go make so dough and come back!");
            // *-- buttons --*
            modalPrimaryBtn.text("Close");
            modalPrimaryBtn.attr('class', 'btn btn-danger');
            modalPrimaryBtn.removeAttr('accesskey');
            // ---- hide secondary button
            modalSecondaryBtn.text("");
            modalSecondaryBtn.attr("class", "btn");
            modalSecondaryBtn.hide();
            modalSecondaryBtn.removeAttr('accesskey');

            // change primary button onClick behavior
            modalPrimaryBtn.on('click', function() {

              // do nothing

            });
          }

          // display a modal to the user
          $('#modal-checkout').modal('toggle');
        }

      }
    });
}

// checkout-handler
