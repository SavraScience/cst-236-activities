<?php

    // imports
    require_once 'inc/misc-functions.php';

    // TODO: Delete all debugging statements
    
    // load all required classes
    // TODO: (this might need to be moved to the VERY top of each file)
    spl_autoload_register(function($class)
    {
        // Register your include directories here:
        $dirs = array(
            "",                     // application directory (current)
            addslashes("../../")    // outside htdocs
        );
        
        // look for classes in each corresponding 'include' directory
         //console_log("Current path: '" . realpath('') . "'");
        foreach($dirs as $dir)
        {
            $baseDir = realpath($dir);
            //console_log("Looking for file in '$baseDir'...");
            //console_log("Does directory exist? " . file_exists($baseDir));
            
            $path = switchSlashes("$baseDir/$class" . '.php');
            //console_log("Does file exist? path = '$path'");
            //console_log("Real path: '" . realpath($path) . "'");
            
            if (file_exists($path))
            {   
                //console_log("Found file at '$path'!");
                require_once $path;
                //require_once $class . ".php";
                return;
            }
        }
    });

?>